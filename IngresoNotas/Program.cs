﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace IngresoNotas
{
    class Program
    {
        static StreamReader Lectura;
        static StreamWriter documento, descartar;
        static int cantidad, n,id;
        static string nombre, cadena;
        static double calificacion1, calificacion2;
        static string[] campos = new string[4];
        static bool encontrar;
        //static string pase;
        static void Main(string[] args)
        {
            //TextWriter archivo;
            //archivo = new StreamWriter("Calificacion.txt");
            //string nombre;
            //double calificacion1, calificacion2, resultado;
            //StreamWriter escribir = File.AppendText("Calificaciones.txt");
            //string pase;
            int opcion1, opcion2;
            do
            {
                Console.Clear();
                Console.WriteLine("==================== SISTEMA DE NOTAS =================");
                Console.WriteLine("de Peñafiel Michael");
                Console.WriteLine("\n 1.- Insertar" +
                                    "\n 2.- Modificar" +
                                    "\n 3.- Eliminar" +
                                    "\n 4.- Consultar " +
                                    "\n 5.- Datos Generales\n" + "digite 6 para finalizar");
                opcion1 = Convert.ToInt32(Console.ReadLine());
                switch (opcion1)
                {
                    case 1:
                        crearArchivo();
                        ingresodeDatos();
                        //Console.WriteLine("Escriba el Nombre del Estudiante");
                        //Estudiante estudiante = new Estudiante();
                        //estudiante.NombreApellido = Console.ReadLine();
                        //nombre = estudiante.NombreApellido;
                        //Console.WriteLine("Digite la primera calificacion");
                        //estudiante.Calificacion = Convert.ToInt32(Console.ReadLine());
                        //calificacion1 = Convert.ToInt32(estudiante.Calificacion);
                        //Console.WriteLine("Digite la segunda calificacion");
                        //estudiante.Calificacion1 = Convert.ToInt32(Console.ReadLine());
                        //calificacion2 = Convert.ToInt32(estudiante.Calificacion1);
                        //Console.WriteLine("Calificacion total");
                        //estudiante.calificacionTotal();
                        //archivo.WriteLine(nombre + " || " + calificacion1 + " || " + calificacion2 + " || ");
                        //archivo.Close();
                        //Console.WriteLine("Pulse ENTER para continuar");
                        //Console.ReadLine();
                        //consulta = File.OpenText("Calificacion.txt");
                        //archivo = File.AppendText("Calificacion.txt");
                        break;
                    case 2:
                        modificar();
                        break;
                    case 3:
                        eliminar();
                        break;
                    case 4:
                        Consultar();
                        break;
                    case 5:
                        Console.Clear();
                        Console.WriteLine("Nombres || Calif1 || Calif2 ");
                        mostrar();

                        break;
                    case 6:
                        break;
                }
            } while (opcion1 != 6);

        }
        static void crearArchivo()
        {
            documento = File.AppendText("Almacenamiento.txt");
            //documento.Close();
        }
        static void ingresodeDatos()
        {
                Console.Clear();
                Console.WriteLine("Digite la cantidad de Estudiantes que desea Ingresar");
                cantidad = Convert.ToInt32(Console.ReadLine());
                for ( n = 1; n <=cantidad ; n++)
                {
                    Estudiante estudiante = new Estudiante();
                    estudiante.id = n;
                    id = estudiante.id;
                    Console.WriteLine("Escriba el Nombre del Estudiante "+ n);
                    estudiante.NombreApellido = Console.ReadLine();
                    nombre = estudiante.NombreApellido;
                    Console.WriteLine("Digite la primera calificacion");
                    estudiante.Calificacion = Convert.ToInt32(Console.ReadLine());
                    calificacion1 = estudiante.Calificacion;
                    Console.WriteLine("Digite la segunda calificacion");
                    estudiante.Calificacion1 = Convert.ToInt32(Console.ReadLine());
                    calificacion2 = estudiante.Calificacion1;
                    documento.WriteLine(id + " | " + nombre + " | " + calificacion1 + " | " + calificacion2);
                    Console.WriteLine("CALIFICACION DEL ESTUDIANTE "+ n +" AGREGADO");
                    Console.ReadLine();
                    
                }
                documento.Close();
            
        }
        static void eliminar()
        {
            Console.Clear();
            string ID;
            StreamWriter borrador;
            encontrar = false;
            char[] espacio = { '|' };
            Lectura = File.OpenText("Almacenamiento.txt");
            borrador = File.CreateText("Borrador.txt");
            Console.WriteLine("Ingresa el Digito a Eliminar");
            ID = Console.ReadLine();
            cadena = Lectura.ReadLine();
            while (cadena != null && encontrar == false)
            {
                campos = cadena.Split(espacio);
                if (campos[0].Trim().Equals(ID))
                {
                    Console.WriteLine("ID: " + campos[0].Trim() +
                                      "\n Estudiante: " + campos[1].Trim() +
                                      "\n Calificacion 1: " + campos[2].Trim() +
                                      "\n calificacion 2: " + campos[3].Trim());
                    encontrar = true;
                    Console.WriteLine("Pulse ENTER para continuar");
                }
                else
                {
                    borrador.WriteLine(cadena);

                }
                cadena = Lectura.ReadLine();
            }
            if (encontrar == false)
            {
                Console.WriteLine("El numero " + ID + " No esta registrado");
            }
            else
            {
                Console.WriteLine("Registro Eliminado");
            }
            Lectura.Close();
            borrador.Close();
            File.Delete("Almacenamiento.txt");
            File.Move("Borrador.txt","Almacenamiento.txt");
            Console.ReadKey(true);


        }
        static void modificar()
        {
            Console.Clear();
            string ID, calificacion1, calificacion2, Modificado;
            StreamWriter borrador;
            encontrar = false;
            char[] espacio = { '|' };
            Lectura = File.OpenText("Almacenamiento.txt");
            borrador = File.CreateText("Borrador.txt");
            Console.WriteLine("Ingresa el Digito para Modificar");
            ID = Console.ReadLine();
            cadena = Lectura.ReadLine();
            while (cadena != null && encontrar == false)
            {
                campos = cadena.Split(espacio);
                if (campos[0].Trim().Equals(ID))
                {
                    encontrar = true;
                    Console.WriteLine("El Estudiante a modificar es el siguiente");
                    Console.WriteLine("ID: " + campos[0].Trim() +
                                      "\n Estudiante: " + campos[1].Trim() +
                                      "\n Calificacion 1: " + campos[2].Trim() +
                                      "\n calificacion 2: " + campos[3].Trim());
                    Console.WriteLine("Ingrese la nueva Calificacion 1:");
                    calificacion1 = Console.ReadLine();
                    Console.WriteLine("Ingrese la nueva Calificacion 2:");
                    calificacion2 = Console.ReadLine();
                    borrador.WriteLine(campos[0] +" | "+campos[1]+ " | " + calificacion1 + " | " + calificacion2);
                    Console.WriteLine("Las calificaciones se han modificado");
                    Console.WriteLine("Pulse ENTER para continuar");
                }
                else
                {
                    borrador.WriteLine(cadena);

                }
                cadena = Lectura.ReadLine();
            }
            if (encontrar == false)
            {
                Console.WriteLine("El numero " + ID + " No esta registrado");
            }
            Lectura.Close();
            borrador.Close();
            File.Delete("Almacenamiento.txt");
            File.Move("Borrador.txt", "Almacenamiento.txt");
            Console.ReadKey(true);


        }
        static void Consultar()
        {
            Console.Clear();
            string ID;
            encontrar = false;
            char[] espacio = { '|' };
            Lectura = File.OpenText("Almacenamiento.txt");
            Console.WriteLine("Ingresa el Digito a consultar");
            ID =Console.ReadLine();
            cadena = Lectura.ReadLine();
            while(cadena != null && encontrar == false)
            {
                campos = cadena.Split(espacio);
                if (campos[0].Trim().Equals(ID))
                {
                    Console.WriteLine("ID: " + campos[0].Trim() +
                                      "\n Estudiante: " + campos[1].Trim() +
                                      "\n Calificacion 1: " + campos[2].Trim() +
                                      "\n calificacion 2: " + campos[3].Trim());
                    encontrar = true;
                    Console.WriteLine("Pulse ENTER para continuar");
                }
                else
                {
                    cadena = Lectura.ReadLine();

                }
            }
            if (encontrar == false)
            {
                Console.WriteLine("El numero " + ID + " No esta registrado");
            }
            Lectura.Close();
            Console.ReadKey(true);

        }
        static void mostrar()
        {
            Lectura = File.OpenText("Almacenamiento.txt");
            cadena = Lectura.ReadLine();
            while (cadena != null)
            {
                Console.WriteLine(cadena);
                cadena = Lectura.ReadLine();
            }
            Console.WriteLine("\n Pulse Enter para Continuar");
            Lectura.Close();
            Console.ReadKey(true);
        }
        //static void otroingresodeDatos()
        //{

        //    do
        //    {
        //        Console.Clear();
        //        for (int n = 1; n >= 1; n++)
        //        {
        //            Console.WriteLine("Escriba el Nombre del Estudiante");
        //            Estudiante estudiante = new Estudiante();
        //            estudiante.NombreApellido = Console.ReadLine();
        //            nombre = estudiante.NombreApellido;
        //            Console.WriteLine("Digite la primera calificacion");
        //            estudiante.Calificacion = Convert.ToInt32(Console.ReadLine());
        //            calificacion1 = estudiante.Calificacion;
        //            Console.WriteLine("Digite la segunda calificacion");
        //            estudiante.Calificacion1 = Convert.ToInt32(Console.ReadLine());
        //            calificacion2 = estudiante.Calificacion1;
        //            documento.WriteLine(n + " || " + nombre + " || " + calificacion1 + " || " + calificacion2);
        //            Console.WriteLine("CALIFICACION DEL ESTUDIANTE AGREGADO");
        //            Console.WriteLine("Desea Ingresar otro estudiante (SI/NO)");
        //        }
        //        pase = Console.ReadLine();
        //        pase = pase.ToUpper();
        //    } while (pase.Equals("SI"));
        //    documento.Close();
        //    Console.ReadKey(true);

        //}
    }
    
}
